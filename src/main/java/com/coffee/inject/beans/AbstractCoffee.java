package com.coffee.inject.beans;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class representing a mapping between Types annotated with Bean
 * and its 'implementation' value.
 *
 * @author Aleksander Studnicki
 */

public abstract class AbstractCoffee implements Coffee {

  private final Map<Class<?>, Class<?>> beans = new HashMap<>();

  /**
   * Add new mapping between Types annotated with Bean and its implementation.
   *
   * @param bean Class annotated with Bean.
   */
  protected void addBean(Class<?> bean) {
    beans.put(bean, bean.getAnnotation(Bean.class).implementation());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T> T brew(final Class<T> bean) {
    if (beans.containsKey(bean)) {
      return coffeeFrom(bean);
    }
    throw new NoSuchBeanException(bean);
  }

  private <T> T coffeeFrom(Class<T> bean) {
    try {
      return castBean(bean, beans.get(bean).getConstructor().newInstance());
    }
    catch (InstantiationException | IllegalAccessException
      | InvocationTargetException | NoSuchMethodException e) {
      throw new BeanNotInitiatedException("Could not brew coffee.", e);
    }
  }

  private <T> T castBean(Class<T> bean, Object concreteBean) {
    if (bean.isInstance(concreteBean)) {
      //noinspection unchecked
      return (T) concreteBean;
    }
    throw new NotAnInstanceException(concreteBean, bean);
  }
}
