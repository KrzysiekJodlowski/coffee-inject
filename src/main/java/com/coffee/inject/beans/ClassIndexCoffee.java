package com.coffee.inject.beans;

/**
 * Used by Coffee interface to map annotated classes found by ClassIndex.
 * {@inheritDoc}
 */

final class ClassIndexCoffee extends AbstractCoffee {

  /**
   * Maps all annotated classes from provided Iterable.
   *
   * @param annotated Iterable with annotated classes.
   * @return Current instance of this class.
   */
  ClassIndexCoffee init(Iterable<Class<?>> annotated) {
    annotated.forEach(this::addBean);
    return this;
  }
}
