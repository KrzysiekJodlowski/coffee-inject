package com.coffee.inject.beans;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

/**
 * Annotation for an interface or a class designed for the dependency injection
 * provided by the Cafe class.
 *
 * @author Aleksander Studnicki
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@IndexAnnotated
public @interface Bean {
  /**
   * An implementation class is passed as the implementation value
   * (must be an instance of the annotated interface/class or exception
   * will be thrown during invoking brew() method in Cafe).
   *
   * @return Implementation class
   */
  Class<?> implementation();
}
