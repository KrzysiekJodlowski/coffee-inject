package com.coffee.inject.beans;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Logger;

import org.scannotation.AnnotationDB;
import org.scannotation.ClasspathUrlFinder;

/**
 * Used by Coffee interface to map annotated classes found with AnnotationDB.
 * {@inheritDoc}
 */

final class ScannotationCoffee extends AbstractCoffee {

  Logger logger = Logger.getLogger(getClass().getName());

  /**
   * Maps all annotated classes with usage of provided
   * AnnotationDB class which scans for .class files.
   *
   * @param db AnnotationDB instance.
   * @return Current instance of this class.
   */
  ScannotationCoffee init(AnnotationDB db) {
    scanArchive(db);

    getBeans(db)
      .stream()
      .map(this::getClassByName)
      .forEach(this::addBean);

    return this;
  }

  private Set<String> getBeans(AnnotationDB db) {
    Set<String> beans = db.getAnnotationIndex().get(Bean.class.getName());
    if (beans == null) {
      return Collections.emptySet();
    }
    return beans;
  }

  private void scanArchive(AnnotationDB db) {
    URL[] urls = ClasspathUrlFinder.findClassPaths();
    try {
      db.scanArchives(urls[0]);
    } catch (IOException e) {
      logger.info(e.getMessage());
    }
  }

  private Class<?> getClassByName(String name) {
    try {
      return Class.forName(name);
    }
    catch (ClassNotFoundException e) {
      throw new BeanNotInitiatedException("Could not initialize coffee.", e);
    }
  }
}
