package com.coffee.inject.beans;

import org.atteo.classindex.ClassIndex;
import org.scannotation.AnnotationDB;

/**
 * Interface returning implementation binded to abstraction on init stage.
 * It also provides default settings for both tools used by CoffeeInject.
 *
 * @author Aleksander Studnicki
 */

public interface Coffee {

  /**
   * Abstraction of a concrete implementation is passed and implementation
   * of binded Class is returned.
   *
   * @param <T> Any Type.
   * @param c Abstraction interface or class marked by @Bean annotation.
   * @return Instance of param passed as an 'implementation'
   *         value in Bean annotation.
   * @throws NoSuchBeanException when instance is not found.
   */
  <T> T brew(Class<T> c);


  /**
   * Maps all classes annotated with Bean using ClassIndex library.
   *
   * @return Class which fills up Iterable object with Bean annotated types.
   */
  static Coffee fromClassIndex() {
    return new ClassIndexCoffee().init(ClassIndex.getAnnotated(Bean.class));
  }

  /**
   * Maps all classes annotated with Bean using Scannotation library.
   *
   * @return Class which fills up map inherited from AbstractCoffee with usage
   *         of AnnotationDB path scanner.
   */
  static Coffee fromScannotation() {
    return new ScannotationCoffee().init(new AnnotationDB());
  }
}
