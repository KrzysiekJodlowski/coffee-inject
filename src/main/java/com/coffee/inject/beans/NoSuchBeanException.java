package com.coffee.inject.beans;

/**
 * Represents situation in which AbstractCoffee mapping does
 * not contain provided class.
 *
 * @author Aleksander Studnicki
 */

public class NoSuchBeanException extends RuntimeException {

  public NoSuchBeanException(Class<?> bean) {
    super(String.format("Bean %s not found.", bean.getName()));
  }
}
