package com.coffee.inject.beans;

/**
 * Represents situation in which provided class is not
 * an instance of a key it is mapped on.
 *
 * @author Aleksander Studnicki
 */

public class NotAnInstanceException extends RuntimeException {

  public NotAnInstanceException(Object providedClass, Class<?> superClass) {
    super(String.format("%s is not an instance of %s.",
        providedClass.getClass().getName(), superClass.getName()));
  }
}
