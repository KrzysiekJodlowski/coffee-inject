package com.coffee.inject.beans;

/**
 * Represents situation in which there is no class annotated with
 * Bean or there is a problem with its instantiation (e.g. there
 * is no access or constructor in that class).
 *
 * @author Aleksander Studnicki
 */

public class BeanNotInitiatedException extends RuntimeException {
  BeanNotInitiatedException(String message, Exception cause) {
    super(message, cause);
  }
}
