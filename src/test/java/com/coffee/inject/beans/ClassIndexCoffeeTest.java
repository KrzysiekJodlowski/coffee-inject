package com.coffee.inject.beans;

import com.coffee.inject.beans.mocks.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ClassIndexCoffeeTest {

  @Test
  public void shouldReturnImplementation() {
    // GIVEN
    ClassIndexCoffee coffee = new ClassIndexCoffee();
    coffee.addBean(CorrectBean.class);

    // WHEN
    CorrectBean bean = coffee.brew(CorrectBean.class);

    // THEN
    assertTrue(bean instanceof CorrectBeanImpl);
  }

  @Test(expectedExceptions = NoSuchBeanException.class)
  public void shouldThrowNoSuchBeanExceptionWhenNoAnnotationWasFound() {
    // GIVEN
    ClassIndexCoffee coffee = new ClassIndexCoffee();
    /*
     * We are testing scenario, when for some reason below method is never called:
     * coffee.addBean(MockBeanInterface.class)
     */

    // WHEN
    // THEN
    coffee.brew(CorrectBean.class);
  }

  @Test(expectedExceptions = NoSuchBeanException.class)
  public void shouldThrowNoSuchBeanExceptionWhenClassIsNotAnnotated() {
    // GIVEN
    ClassIndexCoffee coffee = new ClassIndexCoffee();
    coffee.addBean(CorrectBean.class);

    // WHEN
    // THEN
    coffee.brew(Integer.class);
  }

  @Test(expectedExceptions = RuntimeException.class)
  public void shouldThrowBeanNoInstantiatedExceptionWhenClassHasNoPublicConstructor() {
    // GIVEN
    ClassIndexCoffee coffee = new ClassIndexCoffee();
    coffee.addBean(PrivateConstructorBean.class);

    // WHEN
    // THEN
    coffee.brew(PrivateConstructorBean.class);
  }

  @Test(expectedExceptions = NotAnInstanceException.class)
  public void shouldThrowNotAnInstanceExceptionWhenClassAnnotatedClassIsNotImplementingInterface() {
    // GIVEN
    ClassIndexCoffee coffee = new ClassIndexCoffee();
    coffee.addBean(NotAnInstanceBean.class);

    // WHEN
    // THEN
    coffee.brew(NotAnInstanceBean.class);
  }
}