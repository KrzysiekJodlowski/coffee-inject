package com.coffee.inject.beans;

import com.coffee.inject.beans.mocks.CorrectBean;
import com.coffee.inject.beans.mocks.CorrectBeanImpl;
import com.coffee.inject.beans.mocks.NotAnInstanceBean;
import com.coffee.inject.beans.mocks.PrivateConstructorBean;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ScannotationCoffeeTest {

  @Test
  public void shouldReturnImplementation() {
    // GIVEN
    ScannotationCoffee coffee = new ScannotationCoffee();
    coffee.addBean(CorrectBean.class);

    // WHEN
    CorrectBean bean = coffee.brew(CorrectBean.class);

    // THEN
    assertTrue(bean instanceof CorrectBeanImpl);
  }

  @Test(expectedExceptions = NoSuchBeanException.class)
  public void shouldThrowNoSuchBeanExceptionWhenNoAnnotationWasFound() {
    // GIVEN
    ScannotationCoffee coffee = new ScannotationCoffee();
    /*
     * We are testing scenario, when for some reason below method is never called:
     * coffee.addBean(MockBeanInterface.class)
     */

    // WHEN
    // THEN
    coffee.brew(CorrectBean.class);
  }

  @Test(expectedExceptions = NoSuchBeanException.class)
  public void shouldThrowNoSuchBeanExceptionWhenClassIsNotAnnotated() {
    // GIVEN
    ScannotationCoffee coffee = new ScannotationCoffee();
    coffee.addBean(CorrectBean.class);

    // WHEN
    // THEN
    coffee.brew(Integer.class);
  }

  @Test(expectedExceptions = RuntimeException.class)
  public void shouldThrowBeanNoInstantiatedExceptionWhenClassHasNoPublicConstructor() {
    // GIVEN
    ScannotationCoffee coffee = new ScannotationCoffee();
    coffee.addBean(PrivateConstructorBean.class);

    // WHEN
    // THEN
    coffee.brew(PrivateConstructorBean.class);
  }

  @Test(expectedExceptions = NotAnInstanceException.class)
  public void shouldThrowNotAnInstanceExceptionWhenClassAnnotatedClassIsNotImplementingInterface() {
    // GIVEN
    ScannotationCoffee coffee = new ScannotationCoffee();
    coffee.addBean(NotAnInstanceBean.class);

    // WHEN
    // THEN
    coffee.brew(NotAnInstanceBean.class);
  }
}