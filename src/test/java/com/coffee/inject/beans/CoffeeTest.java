package com.coffee.inject.beans;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CoffeeTest {

  @Test
  public void testFromClassIndex() {
    Assert.assertEquals(Coffee.fromClassIndex().getClass(), ClassIndexCoffee.class);
  }

  @Test
  public void testFromScannotation() {
    Assert.assertEquals(Coffee.fromScannotation().getClass(), ScannotationCoffee.class);
  }
}