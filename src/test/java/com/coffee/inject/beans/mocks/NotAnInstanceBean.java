package com.coffee.inject.beans.mocks;

import com.coffee.inject.beans.Bean;

@Bean(implementation = NotAnInstanceBeanImpl.class)
public interface NotAnInstanceBean {
}
