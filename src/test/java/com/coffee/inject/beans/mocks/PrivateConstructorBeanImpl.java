package com.coffee.inject.beans.mocks;

public class PrivateConstructorBeanImpl implements PrivateConstructorBean {

  // has no public constructor
  private PrivateConstructorBeanImpl() {
  }
}
