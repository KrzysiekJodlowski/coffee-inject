package com.coffee.inject.beans.mocks;

import com.coffee.inject.beans.Bean;

@Bean(implementation = CorrectBeanImpl.class)
public interface CorrectBean {
}
