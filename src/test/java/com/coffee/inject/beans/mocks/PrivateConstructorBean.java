package com.coffee.inject.beans.mocks;

import com.coffee.inject.beans.Bean;

@Bean(implementation = PrivateConstructorBeanImpl.class)
public interface PrivateConstructorBean {
}
